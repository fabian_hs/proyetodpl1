let Lider = require("../../models/Lider");

//Show lideres
exports.lider_list = function(req, res) {
  Lider.allLider(function(err, lideres) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(lideres);
  });
};

//Create lideres
exports.lider_add = function(req, res) {
  Lider.add(
    {
      liderId: req.body.liderID,
      name: req.body.name,
      imagen: req.body.imagen,
      designacion: req.body.designacion,
      abbr: req.body.abbr,
      destacado: req.body.destacado,
      descripcion: req.body.descripcion
    },
    function(err, lider) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(lider);
    }
  );
};

//Update de lideres
exports.lider_update = function(req, res) {
  Lider.findUpdate(
    req.params._id,
    {
      liderId: req.body.liderID,
      name: req.body.name,
      imagen: req.body.imagen,
      designacion: req.body.designacion,
      abbr: req.body.abbr,
      destacado: req.body.destacado,
      descripcion: req.body.descripcion
    },
    function(err, lider) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(lider);
    }
  );
};

//Delete de ideres por ID
exports.lider_delete = function(req, res) {
  Lider.removeById(req.body._id, function(err, lider) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(lider);
  });
};
