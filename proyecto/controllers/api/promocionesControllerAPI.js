let Promociones = require("../../models/Promociones");

//Show promociones
exports.promotion_list = function(req, res) {
  Promociones.allPromocion(function(err, promociones) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(promociones);
  });
};

//Create promociones
exports.promotion_create = function(req, res) {
  Promociones.add(
    {
      Id: req.body.Id,
      nombre: req.body.nombre,
      imagen: req.body.imagen,
      etiqueta: req.body.etiqueta,
      precio: req.body.precio,
      destacado: req.body.destacado,
      descripcion: req.body.descripcion
    },
    function(err, promocion) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(promocion);
    }
  );
};

//Delete By Id promociones
exports.promotion_delete = function(req, res) {
  Promociones.removeById(req.body._id, function(err, promocion) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(promocion);
  });
};

//Update By Id promociones
exports.promotion_update = function(req, res) {
  Promociones.findUpdate(
    req.params._id,
    {
      Id: req.body.Id,
      nombre: req.body.nombre,
      imagen: req.body.imagen,
      etiqueta: req.body.etiqueta,
      precio: req.body.precio,
      destacado: req.body.destacado,
      descripcion: req.body.descripcion
    },
    function(err, promotion) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(promotion);
    }
  );
};
