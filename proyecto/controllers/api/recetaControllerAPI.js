let Recetas = require("../../models/Receta");

// Añadir Recetas sin comentario

exports.Recetas_list = function(req, res) {
  Recetas.allReceta(function(err, Recetases) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(Recetases);
  });
};


exports.Recetas_create = function(req, res) {
  Recetas.add(
    {
      recetaId: req.body.id,
      name: req.body.name,
      image: req.body.image,
      categoria: req.body.categoria,
      etiqueta:req.body.etiqueta,
      precio:req.body.precio,
      destacado: req.body.destacado,
      descripcion: req.body.descripcion
    },
    function(err, Recetas) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(Recetas);
    }
  );
};


exports.Recetas_update = function(req, res) {
  Recetas.findUpdate(
    req.params._id,
    {
      recetaId: req.body.id,
      name: req.body.name,
      image: req.body.image,
      categoria: req.body.categoria,
      etiqueta:req.body.etiqueta,
      precio:req.body.precio,
      destacado: req.body.destacado,
      descripcion: req.body.descripcion
    },
    function(err, Recetas) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(Recetas);
    }
  );
};


exports.Recetas_delete = function(req, res) {
  Recetas.removeById(req.body._id, function(err, Recetas) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(Recetas);
  });
};

// Añadir a las recetas un o varios comentarios
//Create Comments
exports.Recetas_comentario = function(req, res) {
  Recetas.findAndUpdateComments(
    req.params._id,
    {
      $push: {
      comentarios: [
          {
            rating: req.body.rating,
            commentario: req.body.commentario,
            autor: req.body.autor,
            fecha: req.body.fecha
          }
        ]
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};

//Delete Comments
exports.recetasComentario_delete = function(req, res) {
  Recetas.findAndUpdateComments(
    req.params._id,
    {
      $pull: {
        comments: {
          _id: [req.body.comment_id]
        }
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};

//Update By Id Comments
exports.recetasComentario_Update = function(req, res) {
  Dishes.findAndUpdateComments(
    req.params._id,
    req.body.comment_id,
    {
      $set: {
        "comments.$.rating": req.body.rating,
        "comments.$.commentario": req.body.commentario,
        "comments.$.autor": req.body.autor,
        "comments.$.fecha": req.body.fecha
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};
