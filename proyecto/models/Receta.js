let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let recetaSchema = new Schema(
  {
    recetaId: Number,
    name: String,
    imagen: String,
    categoria: String,
    etiqueta: String,
    precio: Number,
    destacado: Boolean,
    descripcion: String,
    comentarios: {
      type: [
        {
          rating:Number,
          commentario:String,
          autor:String,
          fecha:Date
        }
      ],
      index: true
    }
  });


recetaSchema.statics.allReceta = function(cb) {
  return this.find({}, cb);
};


recetaSchema.statics.add = function(receta, cb) {
  return this.create(receta, cb);
};


recetaSchema.statics.findById = function(anId, data, cb) {
  return this.findOne({
    _id: anId
  },data, cb);
};


recetaSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne({
    _id: anId
  }, cb);
};

recetaSchema.statics.findUpdate = function(andId,data,cb){
  return this.findByIdAndUpdate({
    _id: andId
  }, data, cb);
};

recetaSchema.statics.findAndUpdateComments = function(anId, coId, data, cb) {
  return this.findOneAndUpdate(
    {
      _id: anId,// Id de la receta.
      "commentarios._id": coId // Id del commentario.
    },
    data,
    cb
  );
};

module.exports = mongoose.model("receta", recetaSchema);
