let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let promocionSchema = new Schema(
  {
    Id:Number,
    nombre: String,
    imagen: String,
    etiqueta: String,
    precio: Number,
    destacado: Boolean,
    descripcion: String,
  }
);

promocionSchema.statics.allPromocion = function(cb) {
  return this.find({}, cb);
};


promocionSchema.statics.add = function(promocion, cb) {
  return this.create(promocion, cb);
};


promocionSchema.statics.findById = function(anId, data, cb) {
  return this.findOne({
    _id: anId
  },data, cb);
};


promocionSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne({
    _id: anId
  }, cb);
};

promocionSchema.statics.findUpdate = function(andId,data,cb){
  return this.findByIdAndUpdate({
    _id: andId
  }, data, cb);
};

module.exports = mongoose.model("promocion", promocionSchema);
