let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let liderSchema = new Schema(
  {
    liderId: Number,
    name: String,
    imagen: String,
    designacion: String,
    abbr: String,
    destacado: Boolean,
    descripcion: String
  }
);

// Visualizar datos

liderSchema.statics.allLider = function(cb) {
  return this.find({}, cb);
};


// Creacion de los datos

liderSchema.statics.add = function(lider, cb) {
  return this.create(lider, cb);
};


liderSchema.statics.findById = function(anId, data, cb) {
  return this.findOne({
    _id: anId
  },data, cb);
};


// El UPDATE

liderSchema.statics.findUpdate = function(andId,data,cb){
  return this.findByIdAndUpdate({
    _id: andId
  }, data, cb);
};

// Borrar con el Id que generar MOngoo

liderSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne({
    _id: anId
  }, cb);
};


module.exports = mongoose.model("lider", liderSchema);
