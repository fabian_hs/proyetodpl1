let express = require("express");
let router = express.Router();
let lideresControllerAPI = require("../../controllers/api/liderControllerAPI");

router.get("/", lideresControllerAPI.lider_list);

router.post("/create", lideresControllerAPI.lider_add);

router.put("/:_id/update", lideresControllerAPI.lider_update);

router.delete("/delete", lideresControllerAPI.lider_delete);

module.exports = router;
