let express = require('express');

let router = express.Router();

let recetaControllerAPI = require("../../controllers/api/recetaControllerAPI");

router.get("/",recetaControllerAPI.Recetas_list);

router.post("/create", recetaControllerAPI.Recetas_create);

router.delete("/delete", recetaControllerAPI.Recetas_delete );

router.put("/:_id/update", recetaControllerAPI.Recetas_update);

// comentarios

router.post("/:_id/comment", recetaControllerAPI.Recetas_comentario);

//Create Comments
router.delete("/:_id/deleteComment", recetaControllerAPI.recetasComentario_delete);

//Delete Comments
router.put("/:_id/updateComment", recetaControllerAPI.recetasComentario_Update);

module.exports = router;
