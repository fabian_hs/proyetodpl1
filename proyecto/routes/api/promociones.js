let express = require("express");
let router = express.Router();
let promocionesControllerAPI = require("../../controllers/api/promocionesControllerAPI");

// -------- PROMOTIONS -------- //
//Show Promotions
router.get("/", promocionesControllerAPI.promotion_list);

//Create Promotions
router.post("/create", promocionesControllerAPI.promotion_create);

//Delete Promotions
router.delete("/delete", promocionesControllerAPI.promotion_delete);

//Update Promotions
router.put("/:_id/update", promocionesControllerAPI.promotion_update);

module.exports = router;
