var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

//

var recetaAPIRouter = require('./routes/api/receta');
var lideresAPIRouter = require('./routes/api/lideres');
var promocionesAPIRouter = require('./routes/api/promociones');


var mongoose = require('mongoose');// Mongoose

// conexion a la base de datos
mongoose.connect('mongodb://localhost/DPL_Proyecto',{useUnifiedTopology:true,useNewUrlParser:true});
mongoose.Promise= global.Promise;
var db = mongoose.connection;

//
db.on("error", console.error.bind('Error de conexion con MongoDB'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/api/receta',recetaAPIRouter);

app.use("/api/promociones", promocionesAPIRouter);

app.use("/api/lideres", lideresAPIRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
